#pragma once

namespace util
{
    class CancellationToken;
    class ImageDownloader;
    class ImageCache
    {
    public:
        static ImageCache* get();

        void get_image(const std::string& url, const std::function<void(const std::string&)>& on_ready, CancellationToken* cancellation_token);
        static std::string get_image_disk_path(const std::string& url);

    private:
        std::mutex m_mutex;
        std::unordered_map<std::string, util::ImageDownloader*> m_image_downloaders;

        static ImageCache* s_Instance;
    };
}