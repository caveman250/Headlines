#pragma once

namespace util
{
    class HttpClient
    {
    public:
        std::string Get(std::string url, std::vector<std::string> headerValues) const;
        std::string Post(std::string url, std::string data, std::vector<std::string> headerValues) const;
        std::string Post(std::string url, Json::Value argFields, std::vector<std::string> headerValues) const;
        std::string Put(std::string url, Json::Value argFields, std::vector<std::string> headerValues) const;
        std::string Delete(std::string url, std::vector<std::string> headerValues) const;
        bool DownloadImage(std::string url, std::string outFileName, std::vector<std::string> headerValues) const;
        std::string UploadMedia(std::string url, Json::Value argFields, const std::string& fileName, const std::string mimeType) const;
    };
}
