#pragma once

namespace websocketpp
{
    namespace config
    {
        struct asio_tls_client;
    }

    template <typename config>
    class client;
}

typedef websocketpp::client<websocketpp::config::asio_tls_client> client;

namespace util
{
    class WebSocket
    {
    public:
        explicit WebSocket(const std::string& url);
        void SetMessageCallback(std::function<void(std::string)> onMessage);
        void Stop();
        void OnMessage(const std::string& msg);
    private:
        client* m_Client;
        std::function<void(std::string)> m_OnMessage;
        std::thread* m_Thread;
    };
}
