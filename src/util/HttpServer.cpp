#include "HttpServer.h"
#include <microhttpd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

namespace util
{
#define PAGE "<html><head><title>Authorization Successful</title>"\
             "</head><body>You can close this tab now.</body></html>"

    static MHD_Result arg_callback(void *cls,
    enum MHD_ValueKind,
    const char *key,
    const char *value)
    {
        util::HttpServer::IterateArgumentsArgs* args = (util::HttpServer::IterateArgumentsArgs*)cls;
        args->m_Server->OnArgRecieved(key, value);

        return (MHD_Result)1;
    }

    static MHD_Result
    ahc_echo(void * cls,
             struct MHD_Connection * connection,
             const char*,
             const char* method,
             const char*,
             const char*,
             size_t* upload_data_size,
             void** ptr)
    {
        static int dummy;
        util::HttpServer::RunServerArgs* args = (util::HttpServer::RunServerArgs*)cls;
        struct MHD_Response * response;
        int ret;

        if (0 != strcmp(method, "GET"))
            return MHD_NO; /* unexpected method */
        if (&dummy != *ptr)
        {
            /* The first time only the headers are valid,
               do not respond in the first round... */
            *ptr = &dummy;
            return MHD_YES;
        }
        if (0 != *upload_data_size)
            return MHD_NO; /* upload data in a GET!? */
        *ptr = NULL; /* clear context pointer */

        util::HttpServer::IterateArgumentsArgs* getArgsArgs = new util::HttpServer::IterateArgumentsArgs { args->m_Server };
        MHD_get_connection_values(connection, (MHD_ValueKind)(MHD_GET_ARGUMENT_KIND), arg_callback, (void*)getArgsArgs);
        delete getArgsArgs;


        response = MHD_create_response_from_buffer (strlen(args->m_Response),
                                                    (void*) args->m_Response,
                                                    MHD_RESPMEM_PERSISTENT);
        ret = MHD_queue_response(connection,
                                 MHD_HTTP_OK,
                                 response);
        MHD_destroy_response(response);
        return (MHD_Result)ret;
    }

    HttpServer::HttpServer()
            : m_RunServerArgs(nullptr)
            , m_MHDDaemon(nullptr)
            , m_OnArgumentCallback(nullptr)
    {

    }

    void HttpServer::Run(int port)
    {
        m_RunServerArgs = new RunServerArgs {PAGE, this};
        m_MHDDaemon = MHD_start_daemon(MHD_USE_THREAD_PER_CONNECTION,
                                       port,
                                       NULL,
                                       NULL,
                                       &ahc_echo,
                                       m_RunServerArgs,
                                       MHD_OPTION_END);


    }

    void HttpServer::Stop()
    {
        MHD_stop_daemon(m_MHDDaemon);

        delete m_RunServerArgs;
        m_RunServerArgs = nullptr;
    }

    void HttpServer::SetOnArgCallback(std::function<void(std::string, std::string)> cb)
    {
        m_OnArgumentCallback = cb;
    }

    void HttpServer::OnArgRecieved(std::string key, std::string value)
    {
        if (m_OnArgumentCallback)
        {
            m_OnArgumentCallback(key, value);
        }
    }
}
