#include "Popup.h"
#include "Application.h"
#include "UI/Widgets/HeaderBar.h"

namespace ui
{
    Popup::Popup(PageType pageType, bool createAsPage)
        : Page(pageType)
        , m_Actions()
        , m_ForceCreateAsPage(createAsPage)
    {

    }

    Popup::~Popup()
    {
        m_DialogResponseConnection.disconnect();
    }

    void ui::Popup::CreateUI(AdwLeaflet* parent)
    {
        Page::CreateUI(parent);
    }

    void Popup::Show()
    {
        if (VERIFY(m_Dialog, "Attempting to show a popup that hasn't created its ui yet."))
        {
            m_Dialog->show();
        }
    }

    void Popup::Close()
    {
        if (m_Dialog)
        {
            if (m_DialogResponseConnection.connected())
            {
                m_DialogResponseConnection.disconnect();
            }

            m_Dialog->close();
            delete m_Dialog;
            m_Dialog = nullptr;
        }
        else
        {
            Application::Get()->NavigateBackwards();
        }
    }

    void Popup::AddPopupAction(PopupAction action)
    {
        m_Actions.push_back(action);
    }

    void Popup::OnHeaderBarCreated()
    {
        GetHeaderBar()->SetTitle(m_Title);
        for (const PopupAction& action : m_Actions)
        {
            Gtk::Button* button = new Gtk::Button();
            button->set_label(action.m_Label);
            button->signal_clicked().connect([action]()
            {
                action.m_Callback();
            });
            button->set_sensitive(action.m_EnabledByDefault);

            m_HeaderButtons[action.m_ResponseType] = button;

            if (action.m_ResponseType == GTK_RESPONSE_ACCEPT)
            {
                button->get_style_context()->add_class("suggested-action");
                adw_header_bar_pack_end((AdwHeaderBar*)GetHeaderBar()->GetHeaderBar()->gobj(), (GtkWidget*)button->gobj());
            }
            else
            {
                adw_header_bar_pack_start((AdwHeaderBar*)GetHeaderBar()->GetHeaderBar()->gobj(), (GtkWidget*)button->gobj());
            }
        }
    }

    void Popup::CreatePopup(AdwLeaflet* parent, Gtk::Box* box, const std::string& title)
    {
        m_Title = title;
        if (!m_ForceCreateAsPage && !adw_flap_get_folded(Application::Get()->GetFlap()))
        {
            if (Application::Get()->GetPopup(true))
            {
                m_Dialog = new Gtk::Dialog(m_Title, *Application::Get()->GetPopup(true)->m_Dialog, true, true);
            }
            else
            {
                m_Dialog = new Gtk::Dialog(m_Title, *Application::Get()->GetWindow(), true, true);
            }
            m_Dialog->set_default_size(900, 600);
            m_DialogResponseConnection = m_Dialog->signal_response().connect([this](int response)
            {
                OnPopupChoiceSelected((GtkResponseType) response);
            });

            m_Dialog->get_content_area()->append(*box);
            for (const PopupAction& action: m_Actions)
            {
                Gtk::Button* button = m_Dialog->add_button(action.m_Label, action.m_ResponseType);
                if (action.m_ResponseType == GTK_RESPONSE_ACCEPT)
                {
                    button->get_style_context()->add_class("suggested-action");
                }
                button->set_sensitive(action.m_EnabledByDefault);

                m_HeaderButtons[action.m_ResponseType] = button;
            }

            Show();
        }
        else
        {
            m_LeafletPage = adw_leaflet_append(parent, (GtkWidget*)box->gobj());
            adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());
        }
    }

    void Popup::SetHeaderButtonEnabled(GtkResponseType responseType, bool enabled)
    {
        if (m_HeaderButtons.find(responseType) != m_HeaderButtons.end())
        {
            m_HeaderButtons[responseType]->set_sensitive(enabled);
        }
    }
}
