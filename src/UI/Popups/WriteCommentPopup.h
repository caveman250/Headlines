#pragma once

#include "UI/Popups/Popup.h"

namespace ui
{
    class WriteCommentPopup : public Popup
    {
    public:
        explicit WriteCommentPopup(std::function<void(const std::string&)> sendButtonHandler);
        virtual void Cleanup() override;
        virtual void Reload() override {}
        virtual SortType GetSortType() const override { return SortType::None; }
        virtual UISettings GetUISettings() const override;

        Gtk::Box* GetParentCommentBox() { return m_ParentCommentBox; }
        std::string GetText() const { return m_TextView->get_buffer()->get_text(); }
    private:
        void OnPopupChoiceSelected(GtkResponseType response) override;

        void Setup();
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;

        Gtk::Box* m_ParentCommentBox = nullptr;
        Gtk::TextView* m_TextView = nullptr;
        Gtk::Viewport* m_Viewport = nullptr;
        std::function<void(const std::string&)> m_SendButtonHandler;
    };
}
