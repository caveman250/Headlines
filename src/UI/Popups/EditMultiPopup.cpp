#include <API/RedditAPI.h>
#include <AppSettings.h>
#include "EditMultiPopup.h"
#include "util/SimpleThread.h"
#include "util/ThreadWorker.h"
#include "API/MultiReddit.h"
#include "API/Subreddit.h"
#include "API/User.h"
#include "Application.h"
#include "UI/Widgets/SearchResultWidget.h"
#include "UserSubredditsPopup.h"
namespace ui
{
    EditMultiPopup::EditMultiPopup(const api::MultiRedditRef& multireddit)
        : Popup(PageType::CreateMultiReddit, false)
        , m_MultiReddit(multireddit)
        , m_Private(true)
    {
        if (m_MultiReddit)
        {
            m_Private = m_MultiReddit->GetVisibility() == api::MultiReddit::Visibility::Private;
            m_PendingSubreddits = m_MultiReddit->GetSubreddits();
        }
    }

    void EditMultiPopup::Cleanup()
    {
        if (m_ListRoundedCornersSettingChangedHandler != util::s_InvalidSignalHandlerID)
        {
            util::Signals::Get()->RoundedCornersSettingChanged.RemoveHandler(m_ListRoundedCornersSettingChangedHandler);
        }
    }

    UISettings EditMultiPopup::GetUISettings() const
    {
        return UISettings();
    }

    void EditMultiPopup::OnPopupChoiceSelected(GtkResponseType response)
    {
        switch (response)
        {
            case GTK_RESPONSE_ACCEPT:
                Confirm();
                // fall through
            case GTK_RESPONSE_CANCEL:
                Close();
                break;
            default:
                g_warning("SubmitPostPopup::OnPopupChoiceSelected - Unhandled response type: %d", response);
                break;
        }
    }

    Gtk::Box* EditMultiPopup::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/edit_multi_popup.ui");

        Gtk::Box* box = builder->get_widget<Gtk::Box>("MainBox");
        Gtk::Widget* nameWidget = builder->get_widget<Gtk::Widget>("NameListItem");
        FixEntryRowSpacing(nameWidget);
        Gtk::Widget* descWidget = builder->get_widget<Gtk::Widget>("DescListItem");
        FixEntryRowSpacing(descWidget);

        Gtk::Box* widgetsBox = builder->get_widget<Gtk::Box>("ContentBox");
        m_ListBox = builder->get_widget<Gtk::ListBox>("ListBox");
        m_SubredditsListBox = builder->get_widget<Gtk::ListBox>("GetSubredditsAsync");
        if (!AppSettings::Get()->GetBool("rounded_corners", false))
        {
            m_ListBox->get_style_context()->add_class("no_rounded_corners");
            m_SubredditsListBox->get_style_context()->add_class("no_rounded_corners");
        }
        m_ListRoundedCornersSettingChangedHandler = util::Signals::Get()->RoundedCornersSettingChanged.AddHandler([this](bool rounded)
        {
            if (rounded)
            {
                if (m_ListBox->get_style_context()->has_class("no_rounded_corners"))
                {
                    m_ListBox->get_style_context()->remove_class("no_rounded_corners");
                }

                if (m_SubredditsListBox->get_style_context()->has_class("no_rounded_corners"))
                {
                    m_SubredditsListBox->get_style_context()->remove_class("no_rounded_corners");
                }
            }
            else
            {
                if (!m_ListBox->get_style_context()->has_class("no_rounded_corners"))
                {
                    m_ListBox->get_style_context()->add_class("no_rounded_corners");
                }

                if (!m_SubredditsListBox->get_style_context()->has_class("no_rounded_corners"))
                {
                    m_SubredditsListBox->get_style_context()->add_class("no_rounded_corners");
                }
            }
        });

        m_NameEntry = builder->get_widget<Gtk::Entry>("NameEntry");
        m_NameEntry->signal_changed().connect([this]()
        {
            UpdateConfirmBtn();
        });
        m_DescEntry = builder->get_widget<Gtk::Entry>("DescEntry");
        m_DescEntry->signal_changed().connect([this]()
        {
            UpdateConfirmBtn();
        });

        Gtk::Switch* privateSwitch = builder->get_widget<Gtk::Switch>("PrivateSwitch");
        privateSwitch->signal_state_set().connect([this](bool priv) -> bool
        {
            m_Private = priv;
            UpdateConfirmBtn();
            return false;
        }, false);

        if (m_MultiReddit)
        {
            GetSubredditsAsync();
            m_Spinner = new Gtk::Spinner();
            m_Spinner->set_size_request(-1, 100);
            m_Spinner->start();
            widgetsBox->append(*m_Spinner);

            m_NameEntry->set_text(m_MultiReddit->GetDisplayName());
            m_DescEntry->set_text(m_MultiReddit->GetDescription());
        }

        Gtk::Button* addSubButton = builder->get_widget<Gtk::Button>("AddSubredditBtn");
        addSubButton->signal_clicked().connect([this]()
        {
            m_SubsPage = std::make_shared<UserSubredditsPopup>([this](const SearchResultWidget* widget)
            {
                if (m_SubsPage)
                {
                    if (widget->GetSubreddit())
                    {
                        AddSubscription(widget->GetSubreddit()->GetDisplayName());
                        CreateSubredditListItem(widget->GetSubreddit());
                        UpdateConfirmBtn();
                    }

                    m_SubsPage->Close();
                    m_SubsPage = nullptr;
                }
            });
            Application::Get()->AddPage(m_SubsPage);
        });

        AddPopupAction(PopupAction{ "Cancel", GTK_RESPONSE_CANCEL, true, [this]() { Close(); }});
        AddPopupAction(PopupAction{ m_MultiReddit ? "Confirm" : "Create", GTK_RESPONSE_ACCEPT, false, [this](){ Confirm(); Close(); }});

        CreatePopup(parent, box, m_MultiReddit ? "Edit Multireddit" : "Create Multireddit");

        return box;
    }

    void EditMultiPopup::FixEntryRowSpacing(Gtk::Widget* entryWidget)
    {
        Gtk::Widget* actionRowHeader = entryWidget->get_first_child();
        for (Gtk::Widget* child = actionRowHeader->get_first_child(); child != nullptr; child = child->get_next_sibling())
        {
            if (child->get_buildable_id() == "title_box")
            {
                child->set_hexpand(false);
                break;
            }
        }
    }

    void EditMultiPopup::GetSubredditsAsync()
    {
        m_Thread = new util::SimpleThread(
        [this](util::ThreadWorker* worker)
        {
            if (worker->IsCancelled())
            {
                return (void*)nullptr;
            }

            for (const std::string& sub: m_MultiReddit->GetSubreddits())
            {
                if (worker->IsCancelled())
                {
                    return (void*)nullptr;
                }

                if (sub.find("u_") == 0)
                {
                    std::string userName = sub.substr(2);
                    m_Users.push_back(api::RedditAPI::Get()->GetUser(userName));
                }
                else
                {
                    m_Subreddits.push_back(api::RedditAPI::Get()->GetSubreddit(sub));
                }
            }

            return (void*)nullptr;
        },
        [this](void*, bool cancelled, util::SimpleThread*)
        {
            if (!cancelled)
            {
                for (const api::SubredditRef& subreddit: m_Subreddits)
                {
                    CreateSubredditListItem(subreddit);
                }

                for (const api::UserRef& user: m_Users)
                {
                    CreateUserListItem(user);
                }

                for (ui::PageRef& page: Application::Get()->GetAllPagesOfType(PageType::MultiRedditFeed))
                {
                    page->Reload();
                }

                m_Spinner->stop();
            }
        });
    }

    void EditMultiPopup::CreateSubredditListItem(const api::SubredditRef& subreddit)
    {
        ui::SearchResultWidgetRef searchResultWidget = std::make_shared<ui::SearchResultWidget>(subreddit, nullptr);
        searchResultWidget->CreateUI((Gtk::Widget*)m_SubredditsListBox);
        searchResultWidget->AddActionButton("user-trash-symbolic", [this, searchResultWidget]()
        {
            RemoveSubscription(searchResultWidget->GetSubreddit()->GetDisplayName());
            searchResultWidget->RemoveUI();
            UpdateConfirmBtn();
        });
    }

    void EditMultiPopup::CreateUserListItem(const api::UserRef& user)
    {
        ui::SearchResultWidgetRef searchResultWidget = std::make_shared<ui::SearchResultWidget>(user, nullptr);
        searchResultWidget->CreateUI((Gtk::Widget*)m_SubredditsListBox);
        searchResultWidget->AddActionButton("user-trash-symbolic", [this, searchResultWidget]()
        {
            RemoveSubscription(searchResultWidget->GetUser()->GetDisplayName());
            searchResultWidget->RemoveUI();
            UpdateConfirmBtn();
        });
    }

    bool EditMultiPopup::Validate()
    {
        if (m_NameEntry->get_text_length() == 0)
        {
            return false;
        }

        if (!m_MultiReddit)
        {
            return true;
        }

        bool wasPrivate = m_MultiReddit->GetVisibility() == api::MultiReddit::Visibility::Private;
        if (std::string(m_NameEntry->get_text()) != m_MultiReddit->GetDisplayName() ||
            std::string(m_DescEntry->get_text()) != m_MultiReddit->GetDescription() ||
            m_Private != wasPrivate ||
            m_PendingSubreddits != m_MultiReddit->GetSubreddits())
        {
            return true;
        }

        return false;
    }

    void EditMultiPopup::UpdateConfirmBtn()
    {
        SetHeaderButtonEnabled(GtkResponseType::GTK_RESPONSE_ACCEPT, Validate());
    }

    void EditMultiPopup::AddSubscription(std::string subscriptionName)
    {
        m_PendingSubreddits.push_back(subscriptionName);
    }

    void EditMultiPopup::RemoveSubscription(std::string subscriptionName)
    {
        m_PendingSubreddits.erase(std::remove(m_PendingSubreddits.begin(), m_PendingSubreddits.end(), subscriptionName), m_PendingSubreddits.end());
    }

    void EditMultiPopup::Confirm()
    {
        std::string path;
        if (m_MultiReddit)
        {
            path = m_MultiReddit->GetPath();
        }
        else
        {
            std::string userDisplayName = api::RedditAPI::Get()->GetCurrentUser()->GetDisplayName();
            std::string pathName = util::Helpers::StringReplace(m_NameEntry->get_text(), " ", "_");
            std::string userName =  userDisplayName.substr(2, userDisplayName.length() - 2);
            path = util::Helpers::FormatString("/user/%s/m/%s", userName.c_str(), pathName.c_str());
        }

        api::MultiRedditRef multi = api::RedditAPI::Get()->CreateOrUpdateMultiReddit(path,
                                                         m_NameEntry->get_text(),
                                                         m_DescEntry->get_text(),
                                                         m_Private,
                                                         m_PendingSubreddits);

        if (m_MultiReddit)
        {
            m_MultiReddit->SetFromMultiReddit(multi);
        }

        for (ui::PageRef& page : Application::Get()->GetAllPagesOfType(PageType::MultiRedditFeed))
        {
            page->Reload();
        }

        for (ui::PageRef& page : Application::Get()->GetAllPagesOfType(PageType::MultiReddits))
        {
            page->Reload();
        }
    }
}