#include "RedditContentListView.h"
#include "UI/Widgets/RedditContentListItem.h"
#include "AppSettings.h"
#include "util/SimpleThread.h"
#include "UI/ContentProviders/RedditContentProvider.h"
#include "API/RedditAPI.h"

namespace ui
{
    RedditContentListView::RedditContentListView(const RedditContentProviderRef& contentProvider)
    : m_Spinner(nullptr)
    , m_ContentItems()
    , m_ContentProvider(contentProvider)
    , m_Viewport(nullptr)
    , m_FailedToLoadWidget(nullptr)
    , m_LastViewportAdjustment(0)
    , m_FirstVisibleItemIndex(0)
    , m_LastVisibleItemIndex(AppSettings::Get()->GetInt("num_posts_to_load", 5))
    {
    }

    void RedditContentListView::CreateUI(Gtk::Box* parent, Gtk::Viewport* parentViewport)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/list_view.ui");

        m_Viewport = parentViewport;
        m_Viewport->get_vadjustment()->signal_value_changed().connect([this]()
        {
            OnViewportScrolled();
            UpdateVisibleContent();
        });

        Gtk::Widget* clamp = builder->get_widget<Gtk::Widget>("PostsClamp");
        parent->append(*clamp);

        m_ContentContainer = builder->get_widget<Gtk::Box>("PostsView");
    }

    void RedditContentListView::LoadContentAsync(std::function<void()> onFinished)
    {
        if (m_ContentProvider->HasMoreContent())
        {
            m_ContentProvider->GetContentAsync([this, onFinished](std::vector<RedditContentListItemRef> newContent)
            {
                AddContentToUI(newContent);

                if (m_Spinner)
                {
                    m_ContentContainer->remove(*m_Spinner);
                    delete m_Spinner;
                    m_Spinner = nullptr;
                }

                if (onFinished)
                {
                    onFinished();
                }
            }, [this]()
            {
                AddFailedToLoadUI();
            });

            if (!m_Spinner)
            {
                m_Spinner = new Gtk::Spinner();
                m_ContentContainer->append(*m_Spinner);
                m_Spinner->show();
                m_Spinner->set_hexpand(true);
                m_Spinner->set_valign(Gtk::Align::CENTER);
                m_Spinner->set_vexpand(true);
                m_Spinner->set_size_request(-1, 100);
                m_Spinner->start();
            }

            if (m_FailedToLoadWidget)
            {
                m_FailedToLoadWidget->set_visible(false);
            }
        }
    }


    void RedditContentListView::ClearContent()
    {
        m_ContentProvider->Reset();

        if (m_ContentContainer)
        {
            for (RedditContentListItemRef& contentItem : m_ContentItems)
            {
                contentItem->RemoveUI();
                contentItem.reset();
            }
        }

        m_ContentItems.clear();
        m_LastVisibleItemIndex = 9;
        m_FirstVisibleItemIndex = 0;
        m_Viewport->get_vadjustment()->set_value(0);
    }

    void RedditContentListView::AddFailedToLoadUI()
    {
        if (m_FailedToLoadWidget)
        {
            m_FailedToLoadWidget->set_visible(true);
        }
        else
        {
            auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/error_loading.ui");
            m_FailedToLoadWidget = builder->get_widget<Gtk::Overlay>("Overlay");
            Gtk::Label* label = builder->get_widget<Gtk::Label>("Label");
            label->set_text(m_ContentProvider->GetFailedToLoadErrorMsg());
            Gtk::Button* button = builder->get_widget<Gtk::Button>("Button");
            button->signal_clicked().connect([this]()
            {
                m_ContentContainer->remove(*m_FailedToLoadWidget);
                m_FailedToLoadWidget = nullptr;
                LoadContentAsync();
            });
            m_ContentContainer->append(*m_FailedToLoadWidget);
        }

        if (m_Spinner)
        {
            m_ContentContainer->remove(*m_Spinner);
            delete m_Spinner;
            m_Spinner = nullptr;
        }
    }

    void RedditContentListView::OnViewportScrolled()
    {
        if (m_ContentProvider->LoadingContent())
        {
            return;
        }

        auto vAdjustment = m_Viewport->get_vadjustment();
        double distFromBottom = vAdjustment->get_upper() - vAdjustment->get_value();
        int viewportHeight = m_Viewport->get_height();
        if (distFromBottom == viewportHeight)
        {
            LoadContentAsync();
        }
    }

#define VISIBLE_CONTENT_DEBUG_LOGS 0

#if VISIBLE_CONTENT_DEBUG_LOGS
#define VISIBLE_CONTENT_LOG(...) g_info(__VA_ARGS__)
#else
#define VISIBLE_CONTENT_LOG(...)
#endif

void RedditContentListView::UpdateVisibleContent()
{
        if (m_ContentItems.empty())
        {
            return;
        }

        int viewportTop = (int)m_Viewport->get_vadjustment()->get_lower() + (int)m_Viewport->get_vadjustment()->get_value();
        int viewportBottom = viewportTop + m_Viewport->get_height();
        int currAdjustment = (int)m_Viewport->get_vadjustment()->get_value();

        int viewportDelta = currAdjustment < m_LastViewportAdjustment ? -1 : 1;
        int viewportHeight = viewportBottom - viewportTop;

        int start = viewportDelta == 1 ? m_FirstVisibleItemIndex : std::min(m_LastVisibleItemIndex, (int)m_ContentItems.size() - 1);
        int end = viewportDelta == 1 ? (int)m_ContentItems.size() : -1;

        int newFirstVisibleItemIndex = -1;
        int newLastVisibleItemIndex = -1;

        for (int i = start; i != end; viewportDelta == 1 ? ++i : --i)
        {
            ui::RedditContentListItemRef& item = m_ContentItems[i];

            if (item->GetContentBottom() < (viewportTop - viewportHeight) || item->GetContentTop() > (viewportBottom + viewportHeight))
            {
                if (item->GetActive())
                {
                    VISIBLE_CONTENT_LOG("Disabling item at: %d\n- item_top: %d\n- item_bottom: %d\n- cutoff_top: %d\n- cutoff_bottom: %d\n", i, item->GetContentTop(), item->GetContentBottom(), viewportTop - viewportHeight, viewportBottom + viewportHeight);
                    item->SetActive(false);
                }
            }
            else
            {
                if (!item->GetActive())
                {
                    VISIBLE_CONTENT_LOG("Enabling item at: %d\n- item_top: %d\n- item_bottom: %d\n- cutoff_top: %d\n- cutoff_bottom: %d\n", i, item->GetContentTop(), item->GetContentBottom(), viewportTop - viewportHeight, viewportBottom + viewportHeight);
                    item->SetActive(true);
                }


                if (viewportDelta == -1 || newFirstVisibleItemIndex == -1)
                {
                    newFirstVisibleItemIndex = i;
                }

                if (viewportDelta == 1 || newLastVisibleItemIndex == -1)
                {
                    newLastVisibleItemIndex = i;
                }
            }
        }

        m_FirstVisibleItemIndex = newFirstVisibleItemIndex;
        m_LastVisibleItemIndex = newLastVisibleItemIndex;
        VISIBLE_CONTENT_LOG("RedditContentListView::UpdateVisibleContent first: %d", m_FirstVisibleItemIndex);
        VISIBLE_CONTENT_LOG("RedditContentListView::UpdateVisibleContent last: %d", m_LastVisibleItemIndex);
        VISIBLE_CONTENT_LOG("RedditContentListView::UpdateVisibleContent end frame");
        ASSERT(m_FirstVisibleItemIndex != -1 && m_LastVisibleItemIndex != -1, "RedditContentListView::UpdateVisibleContent - Failed to set visible range.");

        m_LastViewportAdjustment = currAdjustment;
}

void RedditContentListView::AddContentToUI(std::vector<RedditContentListItemRef> newContent)
{
     for (const RedditContentListItemRef& item : newContent)
    {
        m_ContentItems.push_back(item);
        item->CreateUI(m_ContentContainer);
    }
}

RedditContentListView::~RedditContentListView()
{
        m_ContentProvider->Reset();
}
}
