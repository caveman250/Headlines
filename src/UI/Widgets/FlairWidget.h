#include "RedditContentListItem.h"
#include "API/api_fwd.h"

namespace ui
{
    class FlairWidget : public RedditContentListItem
    {
    public:
        explicit FlairWidget(const api::FlairRef& flair);
        virtual void CreateUI(Gtk::Widget* parent) override;
        virtual void RemoveUI() override;
        Gtk::Widget* GetWidget() override { return m_Box; }
        virtual int GetContentTop() override { return 0; } // never used
        virtual int GetContentBottom() override { return 0; } // never used

    private:
        api::FlairRef m_Flair;
        Gtk::Box* m_Box;
        Gtk::Box* m_Parent = nullptr;
    };
}
