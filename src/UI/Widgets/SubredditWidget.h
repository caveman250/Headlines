#pragma once

#include "API/api_fwd.h"
#include "util/Signals.h"
#include "UI/ui_types_fwd.h"

namespace ui
{
    class PictureWidget;
    class SubredditWidget
    {
    public:
        ~SubredditWidget();
        void CreateUI(Gtk::Box* parent, bool prepend);
        void SetSubreddit(const api::SubredditRef& subreddit);
        void SetVisible(bool visible);

    private:
        api::SubredditRef m_Subreddit = nullptr;

        PictureWidget* m_IconImageWidget = nullptr;
        Gtk::Box* m_SubredditPanel = nullptr;
        Gtk::Label* m_SubRedditLabel = nullptr;
        Gtk::Box* m_DescriptionBox = nullptr;
        Gtk::Label* m_SubCountLabel = nullptr;
        Gtk::Label* m_CreatedLabel = nullptr;
        Gtk::ToggleButton* m_SubscribeButton = nullptr;

        int m_RoundedCornersSettingChangedHandler = util::s_InvalidSignalHandlerID;
    };
}
