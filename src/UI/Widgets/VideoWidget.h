#pragma once
#include "PictureWidget.h"

namespace ui
{
    class VideoWidget : public PictureWidget
    {
    public:
        VideoWidget(int width, int height, bool crop, bool blur);
    };
}
