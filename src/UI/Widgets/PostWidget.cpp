#include <util/Signals.h>
#include "AppSettings.h"
#include "PostWidget.h"
#include "util/Helpers.h"
#include "util/HtmlParser.h"
#include "Application.h"
#include "HeaderBar.h"
#include "UI/Popups/WriteCommentPopup.h"
#include "API/User.h"
#include "util/SimpleThread.h"
#include "API/Flair.h"
#include "FlairWidget.h"
#include "API/Subreddit.h"
#include "UI/Pages/CommentsPage.h"
#include "UI/Pages/SubredditPage.h"
#include "UI/Pages/UserPage.h"
#include "UI/Pages/ImagePage.h"
#include "UI/Widgets/PictureWidget.h"
#include "UI/Widgets/GifWidget.h"
#include "UI/Widgets/VideoWidget.h"
#include "UI/Pages/VideoPage.h"
#include "API/IconCache.h"
#include "UI/Widgets/AwardWidget.h"
#include "util/ImageCache.h"
#include "util/CancellationToken.h"
#include "util/HtmlMetaParser.h"

namespace ui
{
    PostWidget::PostWidget(const api::PostRef& post)
        : m_Post(post)
          , m_CommentsView(ui::CommentsViewRef())
          , m_MainThreadDispatcher(nullptr)
          , m_Card(nullptr)
          , m_SubredditIconThread(nullptr)
          , m_RoundedCornersSettingChangedHandler(util::s_InvalidSignalHandlerID)
    {
    }

    PostWidget::PostWidget(const api::PostRef& post, const ui::CommentsViewRef& commentsView)
        : m_Post(post)
        , m_CommentsView(commentsView)
        , m_MainThreadDispatcher(nullptr)
        , m_Card(nullptr)
        , m_SubredditIconThread(nullptr)
        , m_RoundedCornersSettingChangedHandler(util::s_InvalidSignalHandlerID)
    {
    }

    PostWidget::~PostWidget()
    {
        if (m_RoundedCornersSettingChangedHandler != util::s_InvalidSignalHandlerID)
        {
            util::Signals::Get()->RoundedCornersSettingChanged.RemoveHandler(m_RoundedCornersSettingChangedHandler);
        }

        if (m_SubredditIconThread)
        {
            m_SubredditIconThread->Cancel();
            m_SubredditIconThread = nullptr;
        }

        if (m_MainThreadDispatcher)
        {
            delete m_MainThreadDispatcher;
        }
    }

    void PostWidget::AddCustomFlair(const std::string& text, const util::Colour& colour, const util::Colour& textColour)
    {
        api::FlairRef flair = std::make_shared<api::Flair>(text, colour, textColour);
        ui::FlairWidgetRef flairUI = std::make_shared<FlairWidget>(flair);
        flairUI->CreateUI(m_FlairBox);
        m_Flairs.push_back(flairUI);
    }

    void PostWidget::CreateUI(Gtk::Widget* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/post.ui");

        SetupCard(builder);
        SetupSubredditIcon(builder);
        SetupFlairs(builder);
        SetupTitle(builder);
        SetupPostContents(builder);
        SetupSubredditLabel(builder);
        SetupUserLabel(builder);
        SetupPostActions(builder);
        SetupDateText(builder);
        SetupAwards(builder);
        ParsePostMetaData();

        ((Gtk::Box*)parent)->append(*m_Card);
    }

    void PostWidget::SetupImage()
    {
        std::vector<util::ImageData> images;

        const std::vector<util::ImageCollection>& postImages = m_Post->GetImagePaths();
        if (!postImages.empty())
        {
            for (const util::ImageCollection& collection: postImages)
            {
                if (collection.m_Images.empty())
                {
                    continue;
                }

                const util::ImageData& imageData = collection.GetImageData(util::ImageQuality::Medium);
                if (IsValidImageUrl(imageData.m_Url))
                {
                    images.push_back(imageData);
                    break;
                }
            }
        }

        if (images.empty() && m_Post->IsVideo())
        {
            util::ImageData imageData;
            imageData.m_Url = "/io/gitlab/caveman250/headlines/missing_image.jpg";
            imageData.m_Width = 502;
            imageData.m_Height = 344;
            images.push_back(imageData);
        }

        bool blurNSFW = AppSettings::Get()->GetBool("hide_nsfw", true);
        bool blurSpoiler = AppSettings::Get()->GetBool("hide_spoiler", true);

        if (!images.empty())
        {
            bool shouldBlur = (blurNSFW && m_Post->IsNSFW()) || (blurSpoiler && m_Post->IsSpoiler());

            const util::ImageData previewImage = images.front();
            if (m_Post->IsVideo())
            {
                m_PostImage = new VideoWidget(previewImage.m_Width, previewImage.m_Height, !m_CommentsView, shouldBlur);
                SetupPictureWidget(previewImage);
            }
            else if (previewImage.m_Url.find(".gif") != std::string::npos || previewImage.m_Url.find(".GIF") != std::string::npos)
            {
                SetupGif(previewImage);
            }
            else
            {
                m_PostImage = new PictureWidget(previewImage.m_Width, previewImage.m_Height, !m_CommentsView, shouldBlur);
                SetupPictureWidget(previewImage);
            }

        }
    }

    void PostWidget::RemoveUI()
    {
        if (m_Card)
        {
            Gtk::Box *box = (Gtk::Box *) m_Card->get_parent();
            box->remove(*m_Card);
            m_Card = nullptr;
        }
    }

    bool PostWidget::IsValidImageUrl(const std::string& imageUrl)
    {
        return !imageUrl.empty() &&
               imageUrl != "self" &&
               imageUrl != "default" &&
               imageUrl != "nsfw" &&
               imageUrl != "spoiler";
    }

    void PostWidget::UpVote()
    {
        if (m_Post->IsDownVoted())
        {
            m_DownVoteImage->get_style_context()->remove_class("downvote");
        }

        m_UpVoteImage->get_style_context()->add_class("upvote");
        m_Post->UpVote();
        UpdateScoreLabel();
    }

    void PostWidget::RemoveUpVote()
    {
        m_UpVoteImage->get_style_context()->remove_class("upvote");
        m_Post->RemoveUpVote();
        UpdateScoreLabel();
    }

    void PostWidget::DownVote()
    {
        if (m_Post->IsUpVoted())
        {
            m_UpVoteImage->get_style_context()->remove_class("upvote");
        }

        m_DownVoteImage->get_style_context()->add_class("downvote");
        m_Post->DownVote();
        UpdateScoreLabel();
    }

    void PostWidget::RemoveDownVote()
    {
        m_DownVoteImage->get_style_context()->remove_class("downvote");
        m_Post->RemoveDownVote();
        UpdateScoreLabel();
    }

    void PostWidget::UpdateScoreLabel()
    {
        if (m_Post->IsUpVoted())
        {
            m_ScoreLabel->get_style_context()->add_class("upvote");
            m_ScoreLabel->get_style_context()->remove_class("downvote");
        }
        else if (m_Post->IsDownVoted())
        {
            m_ScoreLabel->get_style_context()->add_class("downvote");
            m_ScoreLabel->get_style_context()->remove_class("upvote");
        }
        else
        {
            m_ScoreLabel->get_style_context()->remove_class("upvote");
            m_ScoreLabel->get_style_context()->remove_class("downvote");
        }

        std::stringstream ss;
        ss << m_Post->GetScore();
        m_ScoreLabel->set_label(ss.str());
    }

    int PostWidget::GetContentTop()
    {
        return m_Card->get_allocation().get_y();
    }

    int PostWidget::GetContentBottom()
    {
        const Gtk::Allocation& allocation = m_Card->get_allocation();
        return allocation.get_y() + allocation.get_height();
    }

    void PostWidget::SetActive(bool active)
    {
        if (active != m_Active)
        {
            if (m_PostImage)
                m_PostImage->SetActive(active);
            m_SubredditIcon->SetActive(active);

            if (m_GifWidget)
            {
                if (!active)
                {
                    m_GifWidget->pause();
                }
                else if (AppSettings::Get()->GetBool("autoplay_gifs", false))
                {
                    m_GifWidget->play(false);
                }
            }

            m_Card->set_child_visible(active);
        }


        RedditContentListItem::SetActive(active);
    }

    void PostWidget::SetupCard(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_Card = builder->get_widget<Gtk::Box>("Post");
        util::Helpers::ApplyCardStyle(m_Card, AppSettings::Get()->GetBool("rounded_corners", false));
        m_RoundedCornersSettingChangedHandler = util::Signals::Get()->RoundedCornersSettingChanged.AddHandler([this](bool roundedCorners)
        {
            util::Helpers::ApplyCardStyle(m_Card, roundedCorners);
        });
    }

    void PostWidget::SetupSubredditIcon(const Glib::RefPtr<Gtk::Builder> &builder)
    {
        Gtk::Box *subIconBox = builder->get_widget<Gtk::Box>("SubredditPictureBox");

        m_SubredditIcon = new PictureWidget(32, 32, false, false);
        m_SubredditIcon->set_size_request(32, 32);
        subIconBox->append(*m_SubredditIcon);

        m_SubredditIconThread = new util::SimpleThread([this](util::ThreadWorker *worker)
        {
            if (worker->IsCancelled())
            {
                return (void*)nullptr;
            }

            if (!api::IconCache::Get()->IsSubredditIconCached(m_Post->GetSubreddit()))
            {
                api::RedditAPI::Get()->GetSubreddit(m_Post->GetSubreddit());
            }

            return (void*)nullptr;
        },
        [this](void*, bool cancelled, util::SimpleThread*)
        {
            if (!cancelled)
            {
                if (m_SubredditIcon)
                {
                    std::string iconPath = api::IconCache::Get()->GetSubredditIcon(m_Post->GetSubreddit());
                    if (!iconPath.empty())
                    {
                        m_SubredditIcon->SetImageData(
                                util::ImageData(util::Helpers::UnescapeHtml(iconPath)));
                    }
                    else
                    {
                        m_SubredditIcon->SetFromResource("/io/gitlab/caveman250/headlines/default_subreddit.png");
                    }
                }

                m_SubredditIconThread = nullptr; //thread deletes itself.
            }
        });
    }

    void PostWidget::SetupSubredditLabel(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        Gtk::Label* subRedditLabel;
        subRedditLabel = builder->get_widget<Gtk::Label>("SubRedditLabel");
        subRedditLabel->set_label(m_Post->GetSubredditPrefixed());
        Glib::RefPtr<Gtk::GestureClick> gestureClickSubreddit = Gtk::GestureClick::create();
        subRedditLabel->add_controller(gestureClickSubreddit);
        gestureClickSubreddit->signal_released().connect([this](int n_press, double, double)
        {
            if (n_press == 0)
            {
                return;
            }

            struct Storage
            {
                api::SubredditRef subreddit = nullptr;
            };
            m_SubredditIconThread = new util::SimpleThread([this](util::ThreadWorker *worker)
            {
                auto threadStorage = new Storage();
                if (worker->IsCancelled())
                {
                    return (void*)nullptr;
                }
                threadStorage->subreddit = api::RedditAPI::Get()->GetSubreddit(m_Post->GetSubreddit());
                return (void*)threadStorage;
            },
            [this](void* user_data, bool cancelled, util::SimpleThread*)
            {
                auto threadStorage = static_cast<Storage*>(user_data);
                if (cancelled || !threadStorage->subreddit)
                {
                    delete threadStorage;
                    return;
                }

                ui::SubredditPageRef subView = std::make_shared<ui::SubredditPage>(threadStorage->subreddit);
                Application::Get()->AddPage(subView);
                delete threadStorage;
                m_SubredditIconThread = nullptr;
            });
        });
    }

    void PostWidget::SetupUserLabel(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        Gtk::Label* userNameLabel;
        userNameLabel = builder->get_widget<Gtk::Label>("UserLabel");
        userNameLabel->set_markup(m_Post->GetUserNamePrefixed());
        Glib::RefPtr<Gtk::GestureClick> gestureClickUser = Gtk::GestureClick::create();
        userNameLabel->add_controller(gestureClickUser);
        gestureClickUser->signal_released().connect([this](int n_press, double, double)
        {
            if (n_press == 0)
            {
                return;
            }

            ui::UserPageRef userView = std::make_shared<ui::UserPage>(m_Post->GetUserName());
            Application::Get()->AddPage(userView);
        });
    }

    void PostWidget::SetupFlairs(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_FlairBox = builder->get_widget<Gtk::Box>("FlairBox");

        if (m_Post->IsVideo())
        {
            AddCustomFlair("Video", util::Colour(0.1, 0.26, 0.98), util::Colour(1, 1, 1));
        }
        else if (m_Post->IsGallery())
        {
            AddCustomFlair("Gallery", util::Colour(0.1, 0.26, 0.98), util::Colour(1, 1, 1));
        }
        else
        {
            std::string hint = m_Post->GetPostHint();
            if (hint == "link" && !m_Post->GetUrl().empty())
            {
                AddCustomFlair("Link", util::Colour(0.1, 0.26, 0.98), util::Colour(1, 1, 1));
            }
            else if (hint == "image")
            {
                AddCustomFlair("Image", util::Colour(0.1, 0.26, 0.98), util::Colour(1, 1, 1));
            }
            else
            {
                AddCustomFlair("Text", util::Colour(0.1, 0.26, 0.98), util::Colour(1, 1, 1));
            }
        }

        if (!m_Post->GetFlairText().empty())
        {
            api::FlairRef flair = std::make_shared<api::Flair>(m_Post);
            ui::FlairWidgetRef flairUI = std::make_shared<FlairWidget>(flair);
            flairUI->CreateUI(m_FlairBox);
            m_Flairs.push_back(flairUI);
        }
        if (m_Post->IsNSFW())
        {
            AddCustomFlair("NSFW", util::Colour(0.87, 0, 0.44), util::Colour(1, 1, 1));
        }
        if (m_Post->IsSpoiler())
        {
            AddCustomFlair("Spoiler", util::Colour(0.98, 0.1, 0.75), util::Colour(1, 1, 1));
        }

        if (m_Flairs.empty())
        {
            m_FlairBox->set_visible(false);
        }
    }

    void PostWidget::SetupTitle(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        Gtk::Label* titleLabel;
        titleLabel = builder->get_widget<Gtk::Label>("PostTitle");
        titleLabel->set_label(util::Helpers::UnescapeHtml(m_Post->GetTitle()));

        if (!m_CommentsView)
        {
            Glib::RefPtr<Gtk::GestureClick> gestureClick = Gtk::GestureClick::create();
            titleLabel->add_controller(gestureClick);
            gestureClick->signal_released().connect([this](int n_press, double, double)
            {
                if (n_press != 0)
                {
                    ui::CommentsPageRef commentView = std::make_shared<ui::CommentsPage>();
                    Application::Get()->AddPage(commentView);
                    commentView->SetPost(m_Post, "");
                }
            });
        }
    }

    void PostWidget::SetupPostContents(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_ContentsBox = builder->get_widget<Gtk::Box>("ContentsBox");
        SetupSelfText();
        SetupImage();
    }

    void PostWidget::SetupSelfText()
    {
        util::HtmlParser htmlParser;
        if (m_Post->GetUrl().find("reddit") == std::string::npos
        && m_Post->GetUrl().find("redd.it") == std::string::npos)
        {
            auto urlBuilder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/text_label.ui");
            Gtk::Label* label;
            label = urlBuilder->get_widget<Gtk::Label>("Label");
            label->set_margin_top(5);
            label->set_margin_bottom(5);
            label->set_use_markup(true);
            label->signal_activate_link().connect([](const Glib::ustring& url)
            {
                util::Helpers::HandleLink(url);
                return true;
                }, false);

            std::stringstream ss;
            ss << "<a href=\"" << m_Post->GetUrl() << "\">" << m_Post->GetUrl() << "</a>";
            label->set_label(ss.str());
            m_ContentsBox->append(*label);
        }
        else if (!m_Post->GetSelfText().empty() && (m_CommentsView || !AppSettings::Get()->GetBool("hide_text_previews", false)))
        {
            htmlParser.ParseHtml(m_Post->GetSelfText(), m_ContentsBox, m_CommentsView ? -1 : 200, true);

            if (!m_CommentsView)
            {
                for (Gtk::Widget* child = m_ContentsBox->get_first_child(); child != nullptr; child = child->get_next_sibling())
                {
                    if (GTK_IS_LABEL(child->gobj()))
                    {
                        Glib::RefPtr<Gtk::GestureClick> gestureClick = Gtk::GestureClick::create();
                        child->add_controller(gestureClick);
                        gestureClick->signal_released().connect([this](int n_press, double, double)
                        {
                            if (n_press != 0)
                            {
                                ui::CommentsPageRef commentView = std::make_shared<ui::CommentsPage>();
                                Application::Get()->AddPage(commentView);
                                commentView->SetPost(m_Post, "");
                            }
                        });
                    }
                }
            }
        }
    }

    void PostWidget::SetupPostActions(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        SetupVoteButtons(builder);
        SetupCommentsButton(builder);
        SetupShareButton(builder);
        SetupBookmarkButton(builder);
        SetupReplyButton(builder);
        SetupDeleteButton(builder);
    }

    void PostWidget::SetupVoteButtons(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_UpVoteImage = builder->get_widget<Gtk::Image>("UpvoteImage");
        m_DownVoteImage = builder->get_widget<Gtk::Image>("DownvoteImage");

        m_ScoreLabel = builder->get_widget<Gtk::Label>("ScoreText");
        UpdateScoreLabel();
        if (m_Post->IsUpVoted())
        {
            m_UpVoteImage->get_style_context()->add_class("upvote");
        }
        if (m_Post->IsDownVoted())
        {
            m_DownVoteImage->get_style_context()->add_class("downvote");
        }

        Gtk::Button* upVoteButton;
        upVoteButton = builder->get_widget<Gtk::Button>("UpvoteBtn");
        upVoteButton->signal_clicked().connect([this]()
        {
            if (m_Post->IsUpVoted())
            {
                RemoveUpVote();
            }
            else
            {
                UpVote();
            }
        });
        upVoteButton->set_sensitive(api::RedditAPI::Get()->IsLoggedIn());

        Gtk::Button* downVoteButton;
        downVoteButton = builder->get_widget<Gtk::Button>("DownvoteBtn");

        downVoteButton->signal_clicked().connect([this]()
        {
            if (m_Post->IsDownVoted())
            {
                RemoveDownVote();
            }
            else
            {
                DownVote();
            }
        });
        downVoteButton->set_sensitive(api::RedditAPI::Get()->IsLoggedIn());
    }

    void PostWidget::SetupCommentsButton(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        Gtk::Label* numCommentsText;
        numCommentsText = builder->get_widget<Gtk::Label>("NumCommentsText");
        {
            std::ostringstream oss;
            oss << m_Post->GetNumComments();
            numCommentsText->set_label(oss.str());
        }

        Gtk::Button* commentsButton;
        commentsButton = builder->get_widget<Gtk::Button>("CommentsButton");
        if (!m_CommentsView)
        {
            commentsButton->signal_clicked().connect([this]()
            {
                ui::CommentsPageRef commentView = std::make_shared<ui::CommentsPage>();
                Application::Get()->AddPage(commentView);
                commentView->SetPost(m_Post, "");
            });
        }
        else
        {
            commentsButton->set_visible(false);
            numCommentsText->set_visible(false);
        }
    }

    void PostWidget::SetupShareButton(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        Gtk::Button* shareButton;
        shareButton = builder->get_widget<Gtk::Button>("ShareButton");
        if (m_CommentsView)
        {
            shareButton->set_hexpand(true);
        }
        shareButton->signal_clicked().connect([this]()
        {
            Application::Get()->ShowNotification("Link copied to clipboard.");
            std::stringstream ss;
            ss << "https://reddit.com" << m_Post->GetCommentsLink();
            util::Helpers::CopyToClipboard(ss.str());
        });
    }

    void PostWidget::SetupBookmarkButton(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_BookmarkImage = builder->get_widget<Gtk::Image>("BookmarkImage");
        if (m_Post->IsBookmarked())
        {
            m_BookmarkImage->get_style_context()->add_class("remove_bookmark");
        }

        Gtk::Button* bookmarkButton;
        bookmarkButton = builder->get_widget<Gtk::Button>("BookmarkButton");
        bookmarkButton->set_visible(api::RedditAPI::Get()->IsLoggedIn());
        bookmarkButton->signal_clicked().connect([this]()
        {
            if (m_Post->IsBookmarked())
            {
                m_Post->RemoveBookmark();
                Application::Get()->ShowNotification("Post un-saved.");
                m_BookmarkImage->get_style_context()->remove_class("remove_bookmark");
            }
            else
            {
                m_Post->Bookmark();
                Application::Get()->ShowNotification("Post saved.");
                m_BookmarkImage->get_style_context()->add_class("remove_bookmark");
            }
        });
    }

    void PostWidget::SetupReplyButton(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        Gtk::Button* replyButton;
        replyButton = builder->get_widget<Gtk::Button>("ReplyButton");
        replyButton->set_visible(api::RedditAPI::Get()->IsLoggedIn());
        if (m_CommentsView)
        {
            replyButton->signal_clicked().connect([this]()
            {
                auto sendHandler = [this](const std::string& commentText)
                {
                    Json::Value comment = api::RedditAPI::Get()->Comment(m_Post->GetFullID(), commentText);
                    Application::Get()->NavigateBackwards();

                    api::CommentRef newComment = std::make_shared<api::Comment>(comment, "t1");
                    m_CommentsView->InsertUserComment(newComment);
                };

                ui::WriteCommentPageRef replyView = std::make_shared<ui::WriteCommentPopup>(sendHandler);
                Application::Get()->AddPage(replyView);

                util::HtmlParser htmlParser;
                htmlParser.ParseHtml(m_Post->GetTitle(), replyView->GetParentCommentBox(), -1, true);
                if (!m_Post->GetSelfText().empty())
                {
                    htmlParser.ParseHtml(m_Post->GetSelfText(), replyView->GetParentCommentBox(), -1, true);
                }
            });
        }
        else
        {
            replyButton->set_visible(false);
        }
    }

    void PostWidget::SetupDeleteButton(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        Gtk::Button* deleteButton = builder->get_widget<Gtk::Button>("DeleteButton");
        if (!api::RedditAPI::Get()->GetCurrentUser())
        {
            deleteButton->set_visible(false);
            return;
        }

        deleteButton->set_visible(m_Post->GetUserNamePrefixed() == api::RedditAPI::Get()->GetCurrentUser()->GetDisplayNamePrefixed());
        deleteButton->signal_clicked().connect([this]()
        {
            Gtk::MessageDialog* confirmDialog = new Gtk::MessageDialog("Are you sure you want to delete this post?", false, Gtk::MessageType::WARNING, Gtk::ButtonsType::OK_CANCEL);
            confirmDialog->signal_response().connect([this, confirmDialog](int response)
            {
                if (response == Gtk::ResponseType::OK)
                {
                    api::RedditAPI::Get()->Delete(m_Post->GetFullID());
                    m_Card->set_visible(false);
                    if (m_CommentsView)
                    {
                        Application::Get()->NavigateBackwards();
                    }

                    Application::Get()->ShowNotification("Post deleted.");
                }

                delete confirmDialog;
            });

            confirmDialog->set_transient_for(*Application::Get()->GetGtkApplication()->get_active_window());
            confirmDialog->show();
        });
    }

    void PostWidget::SetupDateText(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        Gtk::Label* dateLabel;
        dateLabel = builder->get_widget<Gtk::Label>("DateText");
        dateLabel->set_text(util::Helpers::TimeStampToTimeAndDateString(m_Post->GetTimeStamp()));
    }

    void PostWidget::SetupAwards(const Glib::RefPtr<Gtk::Builder>& builder)
    {
        m_AwardsBox = builder->get_widget<Gtk::FlowBox>("AwardsBox");
        if (!m_Post->GetAwards().empty())
        {
            for (const auto& award: m_Post->GetAwards())
            {
                ui::AwardWidget* awardWidget = new ui::AwardWidget(award);
                m_AwardsBox->insert(*awardWidget, -1);
            }
        }
        else
        {
            m_AwardsBox->set_visible(false);
        }
    }

    void PostWidget::SetupPictureWidget(const util::ImageData& previewImage)
    {
        m_PostImage->SetImageData(previewImage);

        m_ContentsBox->append(*m_PostImage);

        if (m_Post->IsVideo())
        {
            m_PostImage->SetClickHandler([this]()
            {
                ui::VideoPageRef videoPage = std::make_shared<ui::VideoPage>(m_Post->GetTitle(), m_Post->GetVideoUrl());
                Application::Get()->AddPage(videoPage);
            });
        }
        else
        {
            m_PostImage->SetClickHandler([this]()
            {
                Application::Get()->AddPage(std::make_shared<ui::ImagePage>(m_Post->GetImagePaths(), m_Post->GetTitle()));
            });
        }
    }

    void PostWidget::SetupGif(const util::ImageData& previewImage)
    {
        m_GifWidget = new ui::GifWidget(previewImage.m_Width, previewImage.m_Height);
        m_ContentsBox->append(*m_GifWidget);
        m_gif_cancellation_token = new util::CancellationToken();
        util::ImageCache::get()->get_image(m_Post->GetUrl(), [this](const std::string& file_path)
        {
            if (AppSettings::Get()->GetBool("autoplay_gifs", false))
            {
                m_GifWidget->play(false);
            }
            m_GifWidget->set_gif(file_path);
        }, m_gif_cancellation_token);
    }

    void PostWidget::ParsePostMetaData()
    {
        bool shouldParseMeta = (m_Post->GetPostHint() == "link" && !AppSettings::Get()->GetBool("hide_link_previews", false))
                || m_Post->GetPostHint() == "rich:video";
        if (shouldParseMeta)
        {
            new util::SimpleThread([this](util::ThreadWorker*)
            {
                auto meta = util::HtmlMetaParser::get_metadata(m_Post->GetUrl());auto ret = new std::map<std::string, std::string>(meta);
                return (void*)ret;
            },
            [this](void* user_data, bool, util::SimpleThread*)
            {
                bool blurNSFW = AppSettings::Get()->GetBool("hide_nsfw", true);
                bool blurSpoiler = AppSettings::Get()->GetBool("hide_spoiler", true);
                bool shouldBlur = (blurNSFW && m_Post->IsNSFW()) || (blurSpoiler && m_Post->IsSpoiler());

                auto meta = *((std::map<std::string, std::string>*)user_data);
                if (meta.count("og:video") > 0)
                {
                    util::ImageData imageData;
                    imageData.m_Url = meta["og:image"];
                    try
                    {
                        imageData.m_Width = stoi(meta["og:image:width"]);
                        imageData.m_Height = stoi(meta["og:image:height"]);
                    }
                    catch (const std::invalid_argument& e)
                    {
                        imageData.m_Width = -1;
                        imageData.m_Height = -1;
                    }

                    if (!m_PostImage)
                    {
                        m_PostImage = new VideoWidget(imageData.m_Width, imageData.m_Height, !m_CommentsView, shouldBlur);
                        m_ContentsBox->append(*m_PostImage);
                        std::string videoUrl = meta["og:video"];
                        m_PostImage->SetClickHandler([this, videoUrl]()
                        {
                            ui::VideoPageRef videoPage = std::make_shared<ui::VideoPage>(m_Post->GetTitle(), videoUrl);
                            Application::Get()->AddPage(videoPage);
                        });
                    }

                    m_PostImage->SetImageData(imageData);
                }
                else if (meta.count("og:image") > 0)
                {
                    util::ImageData imageData;
                    imageData.m_Url = meta["og:image"];
                    try
                    {
                        imageData.m_Width = stoi(meta["og:image:width"]);
                        imageData.m_Height = stoi(meta["og:image:height"]);
                    }
                    catch (const std::invalid_argument& e)
                    {
                        imageData.m_Width = -1;
                        imageData.m_Height = -1;
                    }

                    std::string mime = meta["og:image:type"];
                    if (mime == "image/gif")
                    {
                        SetupGif(imageData);
                    }
                    else
                    {
                        if (!m_PostImage)
                        {
                            m_PostImage = new PictureWidget(imageData.m_Width, imageData.m_Height, !m_CommentsView, shouldBlur);
                            m_ContentsBox->append(*m_PostImage);
                        }

                        m_PostImage->SetImageData(imageData);

                        //dont override video behavior in cases where we just found a thumbnail.
                        if (dynamic_cast<ui::VideoWidget*>(m_PostImage) == nullptr)
                        {
                            m_PostImage->SetClickHandler([imageData, this]()
                            {
                                std::vector<util::ImageCollection> collections;
                                util::ImageCollection collection;
                                collection.m_Images.push_back(imageData);
                                collections.push_back(collection);
                                Application::Get()->AddPage(std::make_shared<ui::ImagePage>(collections, m_Post->GetTitle()));
                            });
                        }
                    }
                }
            });
        }
    }
}
