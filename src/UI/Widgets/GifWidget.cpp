#include "GifWidget.h"

namespace ui
{
    GifWidget::GifWidget(int width, int height)
            : Gtk::Widget()
    {
        m_aspect_ratio = (float)width / height;

        m_child = new Gtk::Picture();
        m_child->set_parent(*this);

        m_play_button = new Gtk::Image();
        m_play_button->set_from_icon_name("gif");
        m_play_button->add_css_class("video-control");
        m_play_button->set_pixel_size(150);
        m_play_button->set_parent(*this);
        m_play_button->set_visible(false);

        m_spinner = new Gtk::Spinner();
        m_spinner->set_parent(*this);
        m_spinner->set_size_request(100, 100);
        m_spinner->set_halign(Gtk::Align::CENTER);
        m_spinner->set_valign(Gtk::Align::CENTER);
        m_spinner->start();

        Glib::RefPtr<Gtk::GestureClick> gestureClick = Gtk::GestureClick::create();
        add_controller(gestureClick);
        gestureClick->signal_released().connect([this](int n_press, double, double)
        {
            if (n_press == 0)
                return;

            if (!m_animation)
            {
                return;
            }

            if (m_paused)
            {
                play(false);
            }
            else
            {
                pause();
            }
        });
    }

    GifWidget::~GifWidget()
    {
        if (m_timeout_connection.connected())
        {
            m_timeout_connection.disconnect();
        }

        m_child->unparent();
    }

    void GifWidget::set_gif(const std::string& file_path)
    {
        try
        {
            m_animation = Gdk::PixbufAnimation::create_from_file(file_path);

            m_iter = m_animation->get_iter();
            play(m_paused);
            m_spinner->unparent();
            delete m_spinner;
            m_spinner = nullptr;
        }
        catch (std::exception& e)
        {
            std::cout << e.what() << std::endl;
        }
    }

    void GifWidget::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_child)
        {
            m_child->size_allocate(rect, baseline);
        }

        if (m_play_button)
        {
            m_play_button->size_allocate(rect, baseline);
        }

        if (m_spinner)
        {
            m_spinner->size_allocate(rect, baseline);
        }
    }

    void GifWidget::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& , int& ) const
    {
        if (orientation == Gtk::Orientation::VERTICAL)
        {
            if (get_halign() == Gtk::Align::FILL)
            {
                natural = for_size / m_aspect_ratio;
            }
        }

        if (m_child)
        {
            int child_min, child_nat, child_min_baseline, child_nat_baseline;
            m_child->measure(orientation, for_size, child_min, child_nat, child_min_baseline, child_nat_baseline);
            minimum = std::max(minimum, child_min);
            natural = std::max(natural, child_nat);
        }

        if (m_play_button)
        {
            int child_min, child_nat, child_min_baseline, child_nat_baseline;
            m_play_button->measure(orientation, for_size, child_min, child_nat, child_min_baseline, child_nat_baseline);
            minimum = std::max(minimum, child_min);
            natural = std::max(natural, child_nat);
        }

        if (m_spinner)
        {
            int child_min, child_nat, child_min_baseline, child_nat_baseline;
            m_spinner->measure(orientation, for_size, child_min, child_nat, child_min_baseline, child_nat_baseline);
            minimum = std::max(minimum, child_min);
            natural = std::max(natural, child_nat);
        }
    }

    void GifWidget::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_child)
        {
            snapshot_child(*m_child, snapshot);
        }

        if (m_play_button)
        {
            snapshot_child(*m_play_button, snapshot);
        }

        if (m_spinner)
        {
            snapshot_child(*m_spinner, snapshot);
        }
    }

    void GifWidget::play(bool once)
    {
        m_paused = once;
        m_play_button->set_visible(once);

        if (m_animation)
        {
            m_timeout_connection = Glib::signal_timeout().connect([this]()
            {
                get_iter()->advance();
                get_picture()->set_pixbuf(get_iter()->get_pixbuf());
                queue_draw();
                return !m_paused;
            }, m_iter->get_delay_time());
        }
    }

    Gtk::SizeRequestMode GifWidget::get_request_mode_vfunc() const
    {
        return Gtk::SizeRequestMode::WIDTH_FOR_HEIGHT;
    }

    void GifWidget::pause()
    {
        m_play_button->set_visible(true);
        m_paused = true;
    }
}