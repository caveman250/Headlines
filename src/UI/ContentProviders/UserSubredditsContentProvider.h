#pragma once

#include "RedditContentProvider.h"

namespace ui
{
    class UserSubredditsContentProvider : public RedditContentProvider
    {
    public:
        UserSubredditsContentProvider();
        void SetItemClickHandler(std::function<void(const SearchResultWidget*)> onClick);
        void SetReloadRequired(bool reloadRequired) { m_ReloadRequired = reloadRequired; }
    private:
        virtual std::vector<RedditContentListItemRef> GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker) override;
        virtual std::string GetFailedToLoadErrorMsg() override;

        std::function<void(const SearchResultWidget*)> m_OnClick;
        bool m_ReloadRequired = false;
    };
}
