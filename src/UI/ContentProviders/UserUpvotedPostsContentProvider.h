#pragma once
#include "RedditContentProvider.h"

namespace ui
{
    class PostWidget;
    class UserUpvotedPostsContentProvider : public RedditContentProvider
    {
    public:
        UserUpvotedPostsContentProvider();
    private:
        virtual std::vector<RedditContentListItemRef> GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker) override;
        virtual std::string GetFailedToLoadErrorMsg() override;
    };
}
