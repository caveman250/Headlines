#include "VideoPage.h"
#include <libxml/parser.h>
#include <libxml/xmlstring.h>
#include <libxml/tree.h>
#include <util/Helpers.h>
#include <util/HttpClient.h>
#include "AppSettings.h"
#include "UI/Widgets/HeaderBar.h"

namespace ui
{
    VideoPage::VideoPage(const std::string& title, const std::string videoUrl)
        : Page(PageType::VideoPage)
        , m_Title(title)
        , m_VideoUrl(videoUrl)
    {

    }

    Gtk::Box*  VideoPage::CreateUIInternal(AdwLeaflet* parent)
    {
        Gtk::Box* box = new Gtk::Box(Gtk::Orientation::VERTICAL);
        box->set_hexpand(true);
        box->set_vexpand(true);
        m_VideoPlayer = new ui::VideoPlayer(m_VideoUrl);
        m_VideoPlayer->set_vexpand(true);
        box->append(*m_VideoPlayer);

        m_LeafletPage = adw_leaflet_append(parent, (GtkWidget*)box->gobj());
        adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());

        return box;
    }

    void VideoPage::Reload()
    {
        //ignore
    }

    void VideoPage::OnDeactivate()
    {
        if (m_VideoPlayer)
        {
            m_VideoPlayer->pause();
        }
    }

    void VideoPage::Cleanup()
    {

    }

    SortType VideoPage::GetSortType() const
    {
        return SortType::None;
    }

    UISettings VideoPage::GetUISettings() const
    {
        return UISettings
        {
            true,
            false,
            false,
            false,
            false,
            false,
            false,
            false
        };
    }

    void VideoPage::OnHeaderBarCreated()
    {
        GetHeaderBar()->SetTitle(m_Title);
    }
}