#pragma once

#include "Page.h"

namespace ui
{
    class MainFeedPage : public Page
    {
    public:
        MainFeedPage();
        ~MainFeedPage();
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        virtual void Cleanup() override;
        virtual void Reload() override;
        virtual SortType GetSortType() const override { return SortType::Posts; }
        virtual UISettings GetUISettings() const override;

    private:
        virtual void OnHeaderBarCreated() override;

        ui::RedditContentListViewRef m_HomeListView;
        ui::RedditContentListViewRef m_PopularListView;
        ui::RedditContentListViewRef m_AllListView;

        AdwViewStack* m_Stack = nullptr;
        AdwViewSwitcherBar* m_ViewSwitcherBar = nullptr;

        int m_LoginStatusChangedHandler;
    };
}