#include <API/RedditAPI.h>
#include "CommentsPage.h"
#include "UI/CommentsView.h"
#include "API/Post.h"
#include "AppSettings.h"
#include "util/SimpleThread.h"

namespace ui
{
    CommentsPage::CommentsPage()
        : Page(PageType::CommentsPage)
    {

    }

    CommentsPage::CommentsPage(const std::string& postUrl, const std::string& contextComment)
        : Page(PageType::CommentsPage)
        , m_ContextComment(contextComment)
    {
        struct ThreadStorage
        {
            api::PostRef post;
        };
        m_Threads.push_back(new util::SimpleThread([postUrl](util::ThreadWorker* worker)
        {
            auto storage = new ThreadStorage();
            if (!worker->IsCancelled())
            {
                storage->post = api::RedditAPI::Get()->GetPost(postUrl);
            }

            return (void*)storage;
        },
        [this](void* user_data, bool cancelled, util::SimpleThread* thread)
        {
            auto storage = static_cast<ThreadStorage*>(user_data);
            if (!cancelled)
            {
                m_Threads.erase(std::remove(m_Threads.begin(), m_Threads.end(), thread), m_Threads.end());
                SetPost(storage->post, m_ContextComment);
            }
            delete storage;
        }));
    }

    CommentsPage::~CommentsPage()
    {
        for (util::SimpleThread* thread : m_Threads)
        {
            thread->Cancel();
        }

        m_CommentView.reset();
    }

    Gtk::Box* CommentsPage::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/comments_view_page.ui");

        Gtk::Box* box = builder->get_widget<Gtk::Box>("CommentsPage");
        m_LeafletPage = adw_leaflet_append(parent, (GtkWidget*)box->gobj());
        adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());

        m_Viewport = builder->get_widget<Gtk::Viewport>("CommentsViewport");
        Gtk::Box* contentBox = builder->get_widget<Gtk::Box>("CommentsBox");

        m_CommentView = std::make_shared<CommentsView>(CommentsViewType::PostComments);
        m_CommentView->CreateUI(m_Viewport, contentBox);

        return box;
    }

    void CommentsPage::Cleanup()
    {
        m_CommentView->Cleanup();
    }

    void CommentsPage::Reload()
    {
        m_CommentView->Reload();
    }

    SortType CommentsPage::GetSortType() const
    {
        return SortType::Comments;
    }

    UISettings CommentsPage::GetUISettings() const
    {
        return m_CommentView->GetUISettings();
    }

    void CommentsPage::SetPost(const api::PostRef& post, const std::string& contextComment)
    {
        m_ContextComment = contextComment;
        m_CommentView->SetPost(post, m_ContextComment);
        m_CommentView->LoadCommentsAsync(post->GetCommentsLink());
    }
}
