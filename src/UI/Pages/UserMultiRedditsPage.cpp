#include "UserMultiRedditsPage.h"

#include <utility>
#include <AppSettings.h>
#include <Application.h>
#include "UI/ListViews/RedditContentListBoxView.h"
#include "UI/ContentProviders/UserMultiRedditsContentProvider.h"
#include "UI/Widgets/SearchResultWidget.h"
#include "UI/Pages/MultiRedditFeedPage.h"
#include "UI/Widgets/HeaderBar.h"
#include "UI/Popups/EditMultiPopup.h"

namespace ui
{
    UserMultiRedditsPage::UserMultiRedditsPage(std::function<void(const SearchResultWidget*)> onClickOverride)
        : Page(PageType::MultiReddits)
        , m_ContentProvider(std::make_shared<UserMultiRedditsContentProvider>())
        , m_ListView(std::make_shared<ui::RedditContentListBoxView>(m_ContentProvider))
        , m_OnClickOverride(std::move(onClickOverride))
    {

    }

    Gtk::Box* UserMultiRedditsPage::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/posts_view.ui");

        Gtk::Box* box = builder->get_widget<Gtk::Box>("PostsView");
        m_LeafletPage = adw_leaflet_append(parent, (GtkWidget*)box->gobj());
        adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());

        Gtk::Viewport* postsViewport;
        postsViewport = builder->get_widget<Gtk::Viewport>("PostsViewport");
        Gtk::Box* postsBox;
        postsBox = builder->get_widget<Gtk::Box>("PostsBox");
        m_ListView->CreateUI(postsBox, postsViewport);
        m_ListView->GetListBox()->set_sort_func([](Gtk::ListBoxRow* a, Gtk::ListBoxRow* b)
        {
            return a->get_name() > b->get_name();
        });
        m_ContentProvider->SetItemClickHandler([this](const SearchResultWidget* searchResult)
        {
            if (m_OnClickOverride)
            {
                m_OnClickOverride(searchResult);
            }
            else
            {
                ui::MultiRedditFeedPageRef multiRedditPage = std::make_shared<ui::MultiRedditFeedPage>(searchResult->GetMultiReddit());
                Application::Get()->AddPage(multiRedditPage);
            }
        });
        m_ListView->LoadContentAsync();

        return box;
    }

    void UserMultiRedditsPage::Cleanup()
    {

    }

    void UserMultiRedditsPage::Reload()
    {
        m_ListView->ClearContent();
        m_ListView->LoadContentAsync();
    }

    UISettings UserMultiRedditsPage::GetUISettings() const
    {
        return UISettings
        {
            true,
            false,
            false,
            true,
            false,
            true,
            false,
            false
        };
    }

    void UserMultiRedditsPage::OnHeaderBarCreated()
    {
        GetHeaderBar()->AddButton("list-add-symbolic", []()
        {
            EditMultiPopupRef editMultiPopup = std::make_shared<EditMultiPopup>(nullptr);
            Application::Get()->AddPage(editMultiPopup);
        }, false);
    }
}