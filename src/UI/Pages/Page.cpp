#include <Application.h>
#include "Page.h"
#include "UI/Widgets/HeaderBar.h"
#include "UI/Popups/Popup.h"

namespace ui
{
    static int s_IDCounter;
    Page::Page(PageType type)
            : m_PageType(type)
            , m_HeaderBar(nullptr)
    {
        std::stringstream ss;
        ss << "Page" << s_IDCounter++;
        m_ID = ss.str();
    }

    PageType Page::GetPageType() const
    {
        return m_PageType;
    }

    const std::string& Page::GetID() const
    {
        return m_ID;
    }

    void Page::CreateUI(AdwLeaflet* parent)
    {
        m_Box = CreateUIInternal(parent);

        if (m_LeafletPage)
        {
            Application::Get()->ShowPage(this);
            m_HeaderBar = std::make_shared<ui::HeaderBar>(dynamic_cast<Popup*>(this));
            m_HeaderBar->CreateUI(this, GetAllSortTypes());
            OnHeaderBarCreated();
        }
    }

    void Page::AddAction(const std::string& name, const std::function<void()>& cb)
    {
        std::string actionId = util::Helpers::FormatString("%s_action_%d", GetID().c_str(), m_ActionIndex);
        m_ActionIndex++;
        m_Actions.push_back(Application::Get()->GetGtkApplication()->add_action(actionId, cb));
        actionId = util::Helpers::FormatString("app.%s", actionId.c_str());
        m_ActionNames.push_back(std::make_pair(name, actionId));
    }
}