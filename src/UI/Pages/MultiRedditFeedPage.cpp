#include "Application.h"
#include "MultiRedditFeedPage.h"
#include "UI/ContentProviders/MultiRedditContentProvider.h"
#include "UI/ListViews/RedditContentListView.h"
#include "UI/Popups/EditMultiPopup.h"
#include "API/MultiReddit.h"
#include "util/HtmlParser.h"
#include "UI/Widgets/MultiRedditBanner.h"

namespace ui
{

    MultiRedditFeedPage::MultiRedditFeedPage(const api::MultiRedditRef& multiReddit)
     : Page(PageType::MultiRedditFeed)
     , m_ListView(std::make_shared<RedditContentListView>(std::make_shared<MultiRedditContentProvider>(multiReddit)))
    {
        m_MultiReddit = multiReddit;

        AddAction("Edit", [multiReddit]()
        {
            EditMultiPopupRef editMultiPopup = std::make_shared<EditMultiPopup>(multiReddit);
            Application::Get()->AddPage(editMultiPopup);
        });
    }

    MultiRedditFeedPage::~MultiRedditFeedPage()
    {

    }

    Gtk::Box* MultiRedditFeedPage::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/multireddit_page.ui");

        Gtk::Box* box = builder->get_widget<Gtk::Box>("PostsView");
        m_LeafletPage = adw_leaflet_append(parent, (GtkWidget*)box->gobj());
        adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());

        Gtk::Viewport* postsViewport;
        postsViewport = builder->get_widget<Gtk::Viewport>("PostsViewport");
        Gtk::Box* postsBox;
        postsBox = builder->get_widget<Gtk::Box>("PostsBox");

        m_MultiRedditBanner.CreateUI(postsBox, m_MultiReddit);

        m_ListView->CreateUI(postsBox, postsViewport);
        m_ListView->LoadContentAsync();

        return box;
    }

    void MultiRedditFeedPage::Cleanup()
    {
        if (m_RoundedCornersSettingChangedHandler != util::s_InvalidSignalHandlerID)
        {
            util::Signals::Get()->RoundedCornersSettingChanged.RemoveHandler(m_RoundedCornersSettingChangedHandler);
        }
    }

    void MultiRedditFeedPage::Reload()
    {
        m_MultiRedditBanner.OnMultiUpdated(m_MultiReddit);
        m_ListView->ClearContent();
        m_ListView->LoadContentAsync();
    }

    UISettings MultiRedditFeedPage::GetUISettings() const
    {
        return UISettings
        {
            true,
            true,
            false,
            true,
            false,
            true,
            false,
            false
        };
    }
}
