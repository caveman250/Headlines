#pragma once

#include "pch.h"
#include "util/Colour.h"
#include "api_fwd.h"

namespace api
{
    class Flair
    {
    public:
        explicit Flair(const Json::Value& jsonData);
        explicit Flair(const api::PostRef& post);
        Flair(const std::string& text, const util::Colour& colour, const util::Colour& textColour);

        const std::string& GetText() const { return m_Text; }
        const std::string& GetID() const { return m_ID; }
        const util::Colour& GetColour() const { return m_Colour; }
        const util::Colour& GetTextColour() const { return m_TextColour; }
    private:
        std::string m_Text;
        std::string m_ID;
        util::Colour m_Colour;
        util::Colour m_TextColour;
    };
}
